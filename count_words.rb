def count_words(string)
  words = string.scan(/\b\S+\b/)
	result = Hash.new(0)
  words.each { |word| result[word.downcase] += 1 }
  return result
end

puts count_words("A man, a plan, a canal -- Panama")
    # => {'a' => 3, 'man' => 1, 'canal' => 1, 'panama' => 1, 'plan' => 1}
puts count_words "Doo bee doo bee doo"
    # => {'doo' => 3, 'bee' => 2}

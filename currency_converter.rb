class Numeric
  @@currencies = {'yen' => 0.013, 'euro' => 1.292, 'rupee' => 0.019, 'dollar' => 1.0}
  def method_missing(method_id)
    singular_currency = method_id.to_s.gsub( /s$/, '')
    if @@currencies.has_key?(singular_currency)
      self * @@currencies[singular_currency]
    else
      super
    end
  end
  
  def in(to_currency)
    to_currency = to_currency.to_s
    singular_currency = to_currency.gsub(/s$/, '')
      if @@currencies.has_key?(singular_currency)
        self * 1/@@currencies[singular_currency]
      else
        super
      end
  end
end

p 1.rupees.in(:rupees)
p 1.dollar.in(:rupees)
p 1.yen.in(:dollars)
p 1.yen.in(:euros)
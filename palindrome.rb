def palindrome?(str)
  letter = str.gsub(/(\W|\d)/,"")
  letter = letter.downcase
  if (letter == letter.reverse)
    return true
  else
    return false
  end
end

puts palindrome?("A man, a plan, a canal -- Panama")  # => true
puts palindrome?("Abracadabra")

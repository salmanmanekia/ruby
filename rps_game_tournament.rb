class WrongNumberOfPlayersError < StandardError ; end
class NoSuchStrategyError < StandardError ; end

def check_strategy(strategy)
  if strategy == nil
    return false
  elsif !strategy.is_a?(String)
    return false
  elsif !strategy.downcase.match(/[rps]/)
    return false
  else
    return true
  end
end

def rps_game_winner(game)
  raise WrongNumberOfPlayersError unless game.length == 2
  raise NoSuchStrategyError unless check_strategy(game[0][1]) && check_strategy(game[1][1])
    
  winning_formula = ["rs","sp","pr","rr","pp","ss"]
  return winning_formula.index(game[0][1].downcase + game[1][1].downcase) ? game[0] : game[1] 
end

def rps_tournament_winner(tournament)
  if (tournament[0][0].is_a?(Array))
    tournament = [rps_tournament_winner(tournament[0]),rps_tournament_winner(tournament[1])]
  end
    rps_game_winner(tournament)
end

player_list = [["Armando", "P"], ["Dave", "S"]]
puts (rps_game_winner (player_list))

player_list = [
    [
        [ ["Armando", "P"], ["Dave", "S"] ],
        [ ["Richard", "R"],  ["Michael", "S"] ],
    ],
    [
        [ ["Allen", "S"], ["Omer", "P"] ],
        [ ["David E.", "R"], ["Richard X.", "P"] ]
    ]
]
puts rps_tournament_winner(player_list)